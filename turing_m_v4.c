#include <stdlib.h>
#include <stdio.h>
#include <termios.h>
#include <unistd.h>

int i, a, b, x=0, y=0, z=0, v, counter, anz, m, top, countertot;
int *band1, *band2, *band3;
char p;
char ausgabe[31];

int mygetch()
{
	struct termios oldt,
			newt;
	int		ch;
	tcgetattr( STDIN_FILENO, &oldt );
	newt = oldt;
	newt.c_lflag &= ~( ICANON | ECHO );
	tcsetattr( STDIN_FILENO, TCSANOW, &newt );
	ch = getchar();
	tcsetattr( STDIN_FILENO, TCSANOW, &oldt );
	return ch;
}


int ausgabe_band1()
{
	anz=0;
	top=0;
	m=0;
	
	if(i<15)
	{
		for(v=0;v<(15-i);v++)
		{
			printf("X");
			anz++;
		}
	}

	while(band1[top] == '1' || band1[top] == '0')
	{
		m++;
		top++;
	}

	top=0;
	
	while(band1[top] == '1' || band1[top] == '0')
	{
		if(m>16)
		{	
			m--;
			top++;
		}
		else
		{
			printf("%c", band1[top]);
			anz++;
			top++;
		}
	}
	
	if(anz<=31)
	{
		for(v=0;v<(31-anz);v++)
		{
			printf("X");
		}
	}

	printf("\t");

	//Band 2
	anz=0;
	top=0;
	m=0;
	
	if(x<15)
	{
		for(v=0;v<(15-x);v++)
		{
			printf("X");
			anz++;
		}
	}

	while(band2[top] == '1' || band2[top] == '0')
	{
		m++;
		top++;
	}

	top=0;
	
	while(band2[top] == '1' || band2[top] == '0')
	{
		if(m>16)
		{	
			m--;
			top++;
		}
		else
		{
			printf("%c", band2[top]);
			anz++;
			top++;
		}
	}
	
	if(anz<=31)
	{
		for(v=0;v<(31-anz);v++)
		{
			printf("X");
		}
	}

	printf("\t");
	
	//Band 3
	anz=0;
	top=0;
	m=0;
	
	if(y<15)
	{
		for(v=0;v<(15-y);v++)
		{
			printf("X");
			anz++;
		}
	}

	while(band3[top] == '1' || band3[top] == '0')
	{
		m++;
		top++;
	}

	top=0;
	
	while(band3[top] == '1' || band3[top] == '0')
	{
		if(m>16)
		{	
			m--;
			top++;
		}
		else
		{
			printf("%c", band3[top]);
			anz++;
			top++;
		}
	}
	
	if(anz<=31)
	{
		for(v=0;v<(31-anz);v++)
		{
			printf("X");
		}
	}

	
	printf(" Schritt %d\n", counter);
	counter++;
	mygetch();
}

int main()
{
	char c;
	counter=0;
	char test, in1, in2;

	printf("Bitte 2 Zahlen zur Multiplikation auswählen\n");
	in1=mygetch();
	a = in1 - '0';
	printf("Zahl 1: %d\n", a);
	in2=mygetch();
	b = in2 - '0';
	printf("Zahl 2: %d\n", b);

	band1 = malloc((a+b+3)*sizeof(int));
	band2 = malloc((a+b)*sizeof(int));
	band3 = malloc((a*b)*sizeof(int));
	if(band1 == NULL || band2 == NULL || band3 == NULL)
	{
		printf("Error allocating memory!\n");
		return 1;
	}	

	printf("Band 1\t\t\t\tBand 2\t\t\t\tBand 3\n");

	for(i=0;i<a;i++) 
	{
		band1[i]='1';
		ausgabe_band1();
	}

	i = a;
	band1[i] = '0';
	ausgabe_band1();

	for(i=a+1;i<b+a+1;i++)
	{
		band1[i]='1';
		ausgabe_band1();
	}

	x=0;
	i--;
	while(band1[i] == '1')
	{
		band1[i] = '0';
		band2[x] = '1';
		i--;
		x++;
		ausgabe_band1();
	}

	x--;
	y=0;
	i--;
	z=x;
	ausgabe_band1();
	while(band1[i] == '1')
	{
		if(x==z)
		{
			while(band2[x] == '1')
			{
				band3[y] = '1';
				x--;
				y++;
				ausgabe_band1();
			}
			x++;
		}
		else
		{
			while(band2[x] == '1')
			{
				band3[y] = '1';
				x++;
				y++;
				ausgabe_band1();
			}
			x--;
		}
		band1[i] = '0';
		ausgabe_band1();
		i--;
	}

	free(band1);
	free(band2);
	free(band3);
}


