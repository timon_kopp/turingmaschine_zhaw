#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int a, b, counter=0, result=0;
char *band1, *band2, *band3;

int mygetch()
{
        char buffer[30];
        fgets(buffer,28,stdin);
        return atoi(buffer);
}

int ausgabe_band1(int j, int x, int y)
{
        //Band 1
        for(int i=j-15;i<j+16;i++)
        {
          if(i==j)
          {
            printf("[%c]",band1[i]);
          }
          else
          {
            printf("%c",band1[i]);
          }
        }
        printf("\t");

        //Band 2
        for(int i=x-15;i<x+16;i++)
        {
          if(i==x)
          {
            printf("[%c]",band2[i]);
          }
          else
          {
            printf("%c",band2[i]);
          }
        }
        printf("\t");

        //Band 3
        for(int i=y-15;i<y+16;i++)
        {
          if(i==y)
          {
            printf("[%c]",band3[i]);
          }
          else
          {
            printf("%c",band3[i]);
          }
        }
        printf("\t");

        printf(" Schritt %d\n", counter);
        counter++;
        mygetch();
}

int main()
{
        printf("Bitte 2 Zahlen zur Multiplikation auswählen\n");
        a=mygetch();
        printf("Zahl 1: %d\n", a);
        b=mygetch();
        printf("Zahl 2: %d\n", b);

        printf("Allocating memory: Band3: %lu\n", ((unsigned long)a*b)*sizeof(char));
        band1 = malloc((a+b+31)*sizeof(char));
        memset(band1,(int)'X',a+b+31);
        band2 = malloc((b+30)*sizeof(char));
        memset(band2,(int)'X',b+34);
        band3 = malloc((a*b+30)*sizeof(char));
        memset(band3,(int)'X',a*b+34);
        if(band1 == NULL || band2 == NULL || band3 == NULL)
        {
                printf("Error allocating memory!\n");
                return 1;
        }

        printf("Band 1\t\t\t\tBand 2\t\t\t\tBand 3\n");

        for(int j=15;j<a+15;j++)
        {
                band1[j]='1';
        }

        band1[15+a] = '0';

        for(int j=a+16;j<b+a+16;j++)
        {
                band1[j]='1';
        }


        ausgabe_band1(a+b+15,15,15);
        for(int j=a+b+15,x=16;band1[j]=='1';)
        {
                band1[j] = 'X';
                band2[x] = '1';
                j--;
                x++;
                ausgabe_band1(j,x,16);
        }

        band1[15+a]='X';
        ausgabe_band1(15+a,16+b,16);

        for(int j=14+a,y=15,x=15+b,z=15+b;band1[j]=='1';j--)
        {
          if(x==z)
          {
            for(x,y;band2[x]=='1';)
            {
              band3[y]='1';
              x--;
              y++;
              ausgabe_band1(j,x,y);
            }
            x++;
          }
          else
          {
            for(x,y;band2[x]=='1';)
            {
              band3[y]='1';
              x++;
              y++;
              ausgabe_band1(j,x,y);
            }
            x--;
          }
          band1[j]='X';
          ausgabe_band1(j,x,y);
        }

        for(int i=15;band3[i]=='1';i++)
        {
          result++;
        }

        printf("result: %d\n", result);
        printf("step counts: %d\n", counter-1);

        free(band1);
        free(band2);
        free(band3);
}
